// Import nécessaire pour utiliser la classe qui permet de formater l'affichage des nombres
import java.text.DecimalFormat;

public class Music {
    private String title;
    private double duration;
    // Je n'ai pas utilisé d'ArrayList
    private Artist artistSet;

    // Constructeur Music
    public Music(String title, double duration, Artist artistSet) {
        this.title = title;
        this.duration = duration ;
        this.artistSet = artistSet;
    }

    // Methode getter permettant d'accéder au titre de la chanson
    public String getTitle() {
        return title;
    }

    // Methode getter permettant d'accéder à la durée de la chanson
    public double getDuration() {
        return duration;
    }

    // Methode permettant d'afficher les informations de la chanson
    public String getInfos() {
        // Création d'une instance de DecimalFormat au format '00'
        final DecimalFormat decfor = new DecimalFormat("00");
        int minutes = ((int) duration);
        double seconds = ((duration - (int) duration) * 100);
        return title + ", " + decfor.format(minutes) +"m"+ decfor.format(seconds) +"s, "+ artistSet.getFullName();
    }
}
