public class Artist {
    // Declaration des attributs
    private String firstName;
    private String lastName;

    // Constructeur Artist
    public Artist(String firstN, String lastN) {
        this.firstName = firstN;
        this.lastName = lastN;
    }

    // Méthode permettant d'afficher le nom complet de l'artiste
    public String getFullName() {
        return firstName + " " + lastName;
    }
}
