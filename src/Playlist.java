// Imports nécessaires pour utiliser la classe qui permet de formater l'affichage des nombres et les arrayLists
import java.text.DecimalFormat;
import java.util.ArrayList;

public class Playlist {
    private Music currentMusic;
    public ArrayList<Music> musicList;
    private double totalDuration;

    // Constructeur Playlist
    public Playlist(Music currentMusic, ArrayList<Music> musicList) {
        this.currentMusic = currentMusic;
        this.musicList = musicList;
    }

    // Methode permettant d'ajouter des chansons à la playlist
    public void add(Music music) {
        musicList.add(music);
    }

    // Methode permettant de retirer des chansons à la playlist
    public void remove(int position) {
        musicList.remove(position);
    }

    //Methode permettant de calculer la durée totale de la playlist
    public String getTotalDuration() {
         // Création d'une instance de DecimalFormat au format '00'
         final DecimalFormat decfor = new DecimalFormat("00");
         double totalDurationSec = 0.0;
         // ForEach
         for(var song : musicList) {
         // Conversion de la durée d'une chanson en secondes
             double duration = song.getDuration();
             int minutesToSeconds = ((int) duration) * 60;
             double secondsLeft = ((duration - (int) duration) * 100);
             double totalInSecond = minutesToSeconds + secondsLeft;
             totalDuration += totalInSecond;
         }
         // Condition pour l'affichage des différents messages
         if(totalDuration>0) {
             // Utilisation de l'instance de DecimalFormat pour apliquer le format aux résultats numériques
             return "Durée totale de la playlist : " + decfor.format((int)totalDuration/60) + "m" + decfor.format((int)totalDuration%60) + "s.";
         } else {
             return "Votre playlist est vide.";
         }
    }

    // Methode permettant de passer à la chanson suivante dans la liste de lecture
    public String next() {
        String message = "";
        // Si aucune chanson n'est en cours de lecture, alors lire la première chanson de musicList
        if(currentMusic == null) {
            currentMusic = musicList.get(0);
            message = "Vous écoutez " + currentMusic.getTitle() + ".";
        // Si une chanson est en cours de lecture, alors lire la suivante...
        } else {
            int index = musicList.indexOf(currentMusic);
            if(index < musicList.size()-1) {
                currentMusic = musicList.get(index+1);
                message = "Vous écoutez " + currentMusic.getTitle() + ".";
            // ...à moins que ce soit la dernière de musicList, dans ce cas-là afficher un message.
            } else {
                currentMusic = null;
                message = "La playlist est finie.";
            }
        }
        return message;
    }
}
