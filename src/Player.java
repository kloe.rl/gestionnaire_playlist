import java.util.ArrayList;

public class Player {
    public static void main(String[] args) {

        Artist PatrickSebastien = new Artist("Patrick", "Sebastien");
        Artist FelicienTaris = new Artist("Felicien", "Taris");
        Artist LosDelRio = new Artist("Los", "Del Rio");
        Artist Vengaboys = new Artist("Venga", "Boys");

        Music BonhommeMousse = new Music("Le petit bonhomme en mousse", 3.02, PatrickSebastien);
        Music CumCumMania = new Music("Cum Cum Mania", 3.34, FelicienTaris);
        Music LaMacarena = new Music("La Macarena", 6.10, LosDelRio);
        Music BoomBoomBoomBoom = new Music("Boom Boom Boom Boom!", 3.26, Vengaboys);

        Playlist MegaMixFiesta = new Playlist(null, new ArrayList<Music>());

        //System.out.println(PatrickSebastien.getFullName());
        System.out.println(BonhommeMousse.getInfos());
        //System.out.println(CumCumMania.getInfos());
        //System.out.println(Macarena.getInfos());
        //System.out.println(BoomBoomBoomBoom.getInfos());’
        MegaMixFiesta.add(BonhommeMousse);
        MegaMixFiesta.add(CumCumMania);
        MegaMixFiesta.add(LaMacarena);
        MegaMixFiesta.add(BoomBoomBoomBoom);
        //PlaylistFiesta.remove(0);
        System.out.println(MegaMixFiesta.getTotalDuration());
        System.out.println(MegaMixFiesta.next());
        System.out.println(MegaMixFiesta.next());
        System.out.println(MegaMixFiesta.next());
        System.out.println(MegaMixFiesta.next());
        System.out.println(MegaMixFiesta.next());
    }
}